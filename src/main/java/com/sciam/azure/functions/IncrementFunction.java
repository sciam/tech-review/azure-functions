package com.sciam.azure.functions;

import com.microsoft.azure.functions.*;
import com.microsoft.azure.functions.annotation.AuthorizationLevel;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpTrigger;

import java.util.Optional;

public class IncrementFunction {

    private int counter = 0;
    private static int STATIC_COUNTER = 0;

    /**
     * This function listens at endpoint "/api/increment". Invoke it using "curl" command in bash:
     * curl {your host}/api/increment
     */
    @FunctionName("increment")
    public HttpResponseMessage run(
            @HttpTrigger(name = "req", methods = {HttpMethod.GET}, authLevel = AuthorizationLevel.ANONYMOUS) HttpRequestMessage<Optional<String>> request,
            final ExecutionContext context) {
        context.getLogger().info("Increment trigger processed a request.");
        this.counter++;
        STATIC_COUNTER++;
        return request
                .createResponseBuilder(HttpStatus.OK)
                .header("Content-Type", "application/json")
                .body(String.format("{ attributeValue: %d , staticValue : %d, comment: 'live demo' }", this.counter, STATIC_COUNTER))
                .build();
    }
}
