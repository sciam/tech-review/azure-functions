# Tech Review: Comprendre le Devops grâce au cloud.

> Date: 18 Septembre 2020
>
> Auteur: Alexandre Lewandowski
>
> URL de la vidéo: [#1 Fonction (Lambdas) et DevOps platform sur Microsoft Azure](https://caitsconsulting.sharepoint.com/:v:/s/SCIAMCommunicationclients/EeTY-WE_dRhJrlK39x_E6ooB6GQtyPUDqx5ANjx1Lt03gw?e=cKNZBD)

L'adoption des bonnes pratiques de développement logiciel (XP, TDD, ...) a permis d'augmenter la qualité des logiciels. 

Les pratiques Devops (CI/CD, Infra as Code, conteneurs...) ont permis de réduire le "lead time to deliver" et une 
gestion simplifiée des environnements de production.
 
Aujourd'hui ces concepts sont mis en application par les fournisseurs de Cloud à une échelle difficilement égalable . 

Durant cette Tech Review, nous mettrons en applications ces concepts au travers d'une architecture _**Serverless**_ tout
en bénéficiant de services supplémentaires offerts le cloud. Pour cela nous utiliserons le Cloud Azure qui permet 
d'expérimenter l'approche gratuitement !

# Le package 

Vous aussi mettez en oeuvre une fonction Azure. Avant de commencer :
- [Créez votre compte azure](docs/azure-subscription.md)
- [Installez et configurer votre environnement de développement](docs/configurer-environnement-dev.md) 


## Cloner le repository

Ouvrez intellIJ et clonez le repository : 

"File" > "New" > "Project from Version Control..." >
URL https://gitlab.com/sciam/tech-review/azure-functions.git > "Clone"


![Clonez le code source ](docs/img/clone-repo.png (250x250))

## Connectez-vous à Azure

Depuis IntellIJ, entrez vos informations d'identification au portail Azure.

Ouvrez l'onglet "Azure Explorer" sur la gauche de l'IDE puis cliquer sur l'icône de connection. 

![Initiez le login depuis intellIJ](docs/img/intellij-azure-login.png (250x250))

Suivez les instructions, par défaut vous pouvez-vous connecter en utilisant le device login et cliquez "Sign in".

![Selectionnez le "Device Login"](docs/img/intellij-azure-login-selection-device.png (250x250))

Une fenêtre vous demandera de vous identifier dans un navigateur. Cliquez sur "Open&Copy".

![Copier le code d'authentification](docs/img/intellij-azure-login-copy-open.png (250x250))

Collez le code dans le champ et suivez les instructions jusqu'à obtenir la fenêtre de confirmation.

![Collez le code dans votre navigateur](docs/img/intellij-azure-login-popup-authentication-code.png (250x250))

![Compléter les instructions jusqu'à confirmation de l'authentification](docs/img/intellij-azure-login-confirmation.png (250x250))

Depuis intellIJ, sélectionnez votre souscription gratuite. 

![Sélectionnez votre souscription gratuite](docs/img/intellij-selection-azure-subscription.png (250x250))

L'explorateur Azure apparait comme authentifié:

![Explorateur azure connecté](docs/img/intellij-azure-login-verification-intellij.png (250x250))


## Créez votre projet depuis intellIJ

Depuis intellIJ, créer un nouveau projet. Cliquez sur "File" > "New" > "Project..."

![Créez votre projet](docs/img/projet-nouveau.png (250x250))

Sélectionner le template "Azure Functions", java en version 11 et le Triggers "HttpTrigger" puis cliquez sur "Next".

![Choisissez le template de projet](docs/img/projet-azure-fucntions-conf.png (250x250))

Remplissez les champs pour la configuration Maven, notamment le "Group" qui correspond au GroupId Maven et le 
"Package name" qui correspondra au package java pour le projet. 

![Configuration Maven](docs/img/projet-maven-conf.png (250x250))

Ensuite cliquer sur "Finish" pour créer le projet. Vous pouvez aussi choisir d'ouvrir le nouveau projet dans la fenêtre 
courante ou une nouvelle fenêtre intellIJ.  

![Cliquez sur "Finish"](docs/img/projet-finish.png (250x250))
![Choisissez d'ouvrir dans une nouvelle fenêtre](docs/img/projet-nouvelle-fenetre.png (250x250))

Vous obtiendrez un squelette de projet contenant une Fonction Azure "HttpTriggerFunction". 

![Squellette de projet](docs/img/projet-squelette.png (250x250))

Pour l'exécuter en locale, cliquez sur la flèche verte à gauche de la déclaration de la méthode java "run".

![Démarrez le projet en local](docs/img/projet-start-locally.png (250x250))

Dans la console, vous verrez l'exécution se lancer. Vérifiez que la fonction est bien détectée. La console affiche la 
liste des fonctions existantes. 

```
Now listening on: http://0.0.0.0:7071
Application started. Press Ctrl+C to shut down.

Functions:

	HttpTrigger-Java: [GET,POST] http://localhost:7071/api/HttpTrigger-Java
``` 
 
 ![Le fonction Azure s'exécuteur en local](docs/img/projet-local-exec.png (250x250))
 
 Vous pouvez tester son fonctionnement grâce à des commandes curl. 
 
 ```bash
alex@LAPTOP ~ $ curl -s http://localhost:7071/api/HttpTrigger-Java
Please pass a name on the query string or in the request body

alex@LAPTOP ~ $ curl -s -X GET http://localhost:7071/api/HttpTrigger-Java?name=Alex
Hello, Alex 

alex@LAPTOP ~ $ curl -s -X POST http://localhost:7071/api/HttpTrigger-Java?name=Alex -d "Alex"
Hello, Alex
```
 
## Créez une "application de fonction" depuis le portail Azure 
 
Avant de pouvoir déployer votre fonction, il vous faudra créer depuis le portail une application de fonction.
Pour cela, connectez-vous sur le portail azure, puis recherchez "Application de fonction" et cliquer sur la suggestion 
qui correspond pour accéder à l'interface de création d'application de fonction. 

![Accédez à l'interface "Application de fonction"](docs/img/azure-creer-application-de-donction.png (250x250))

Cliquez sur "Ajouter".

![Ajoutez une "Application de fonction"](docs/img/azure-ajouter-application-de-donction.png (250x250))

Remplissez le formulaire de base :
- Sélectionnez votre Abonnement (Free Trial si vous venez de créer votre compte)
- Créer un groupe de ressource. Exemple : "tech-review". Attention une validation d'unicité vous oblige à choisir un nom
qui n'existe pas déjà
- Remplissez le champ "Nom de l'application de fonction". Ce sera aussi l'URL pour accéder à votre API
- Pour le champ "Publier" sélectionnez "Code"
- "Pile d'exécution" sélectionnez "Java" 
- "Version" , sélectionnez "11.0 (Préversion)"
- "Région", choisissez une région proche de chez vous ;-)
- Cliquez sur "Suivant : Hébergement >"

![Remplissez le formulaire de base](docs/img/azure-formulaire-de-base.png (250x250))


Remplissez le formulaire Hébergement :
- Créer une compte de stockage (attention le nom doit être unique)
- Système "windows"
- "Type de plan" sélectionnez "Consommation (serverless)"
- Cliquez sur "Suivant : Surveillance"

![Remplissez le formulaire Hébergement](docs/img/azure-formulaire-hebergement.png (250x250))

Remplissez le formulaire Surveillance :
- "Activer Application Insights" sélectionnez "Oui"
- "Application Insights" créez une nouvelle instance avec un nom unique.  
- Cliquez sur "Suivant : Balises"

![Remplissez le formulaire Surveillance](docs/img/azure-formulaire-surveillance.png (250x250))

Remplissez le formulaire Balises :
- Créez un tag nommé "techreview" avec pour valeur "compute".
    - dans la colone resources, sélectionnez "Application de fonction", "Application Insights", "Plan App Services".
- Créez un tag nommé "techreview" avec pour valeur "storage".
    - Dans la colone resources, sélectionnez "Compte de stockage"
- Cliquez sur "Suivant : Vérifier + créer >"


![créez le tag compute](docs/img/azure-formulaire-balises-tag-compute.png (250x250))
![créez le tag compute](docs/img/azure-formulaire-balises-tag-storage.png (250x250))

Un récapitulatif vous est proposé avant de finaliser le déploiement. Aussi, en cas d'erreur sur l'unicité des noms, 
cette étape peut échouer en vous indiquant l'erreur de validation. Dans ce cas, revenez à l'étape qui n'est pas valide 
pour mettre des valeurs valides ou c'est nécessaire.  

![Vérifiez votre demande de déploiement ](docs/img/azure-formulaire-recap.png (250x250))

Si la configuration vous convient, cliquez sur "Créer". Votre demande de déploiement est alors soumise.

![Soumettez votre demande de déploiement ](docs/img/azure-formulaire-soumission.png (250x250))

La page s'actualise jusqu'à ce que votre déploiement soit terminé. 

![Votre application de fonction est déployée ](docs/img/azure-app-deployee.png (250x250))
 
## Déployez votre "Azure function" depuis intellIJ

Depuis intellIJ, faites un clic droit sur le projet > "Azure" > "Deploy to Azure functions". 

![Demandez le deploiment de votre fonction sur le cloud Azure](docs/img/azure-deploy-depuis-intellij.png (250x250))
 
Dans le champ "Function", Sélectionnez l'application de fonction sur laquelle vous voulez déployer vos fonctions. Celle 
que vous venez de créer. Cliquez sur "Run".
 
![déployez votre fonction sur le cloud Azure](docs/img/azure-deploiement-selection-app.png (250x250))
 
Vous pouvez suivre l'avancement du déploiement depuis la console d'intellIJ. Si le déploiement se déroule bien vous 
devez obtenir un message `Deploy succeed`

![Votre déploiement est terminé](docs/img/azure-deploiement-termine.png (250x250))

Vous pouvez tester son fonctionnement grâce à des commandes curl. Pour cela reprenez l'URL proposée dans les log de 
déploiement disponibles depuis la console intellIJ.  
 
 ```bash
alex@LAPTOP ~ $ curl -s https://tech-review.azurewebsites.net/api/httptrigger-java
Please pass a name on the query string or in the request body

alex@LAPTOP ~ $ curl -s -X GET https://tech-review.azurewebsites.net/api/httptrigger-java?name=Alex
Hello, Alex 

alex@LAPTOP ~ $ curl -s -X POST https://tech-review.azurewebsites.net/api/httptrigger-java?name=Alex -d "Alex"
Hello, Alex
```

## Créez un pipeline de CI / CD

Consultez la page dédiée pour [créer un pipeline azure devops](docs/creer-un-pipeline-azure-devops.md).


