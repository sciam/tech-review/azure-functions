# Créer un pipeline Azure Devops

Accédez au portail des développeurs : https://dev.azure.com/

## Créer un nouveau projet
 
![Créer une projet dans le portail azure devops ](img/azure-devops-creer-projet.png)

Remplissez le nom de projet, choisissez la visibilité. Pour le gestionnaire de source choisissez "Git".
Work item process selon votre préférence. Enfin cliquez sur Créer.

Votre Espace de travail est prêt.

![Votre espace azure devops est prêt](img/azure-devops-projet-pret.png)

## Créer le pipeline

### Initialiser le pipeline
Ouvrez le Menu "Pipeline" sur la gauche de l'écran et cliquez sur "Create Pipeline".

![Créez votre pipeline](img/azure-devops-creer-pipeline.png)

Pour la connection au gestionnaire de code, choisissez "Other Git"
![Créez votre pipeline](img/azure-devops-choisir-git.png)

Cliquer sur "Add connection".
![Ajouter une connection](img/azure-devops-git-source.png)

Remplissez le formulaire en indiquant l'URL de votre projet. Si vous n'avez pas de gestionnaire de code, vous pouvez 
pointer sur le nôtre pour tester le pipeline Azure DevOps : https://gitlab.com/sciam/tech-review/azure-functions.git

Ce repository étant publique il n'est pas nécessaire de fournir d'identifiant. Cliquez sur OK.
![Configurez git](img/azure-devops-git-config.png)

Configurez la branche par défaut pour être "master".
![branche par défaut master](img/azure-devops-git-default-branch.png)

### Créez le binaire avec Maven

Ajouter une ache à votre pipeline. 
Choisissez le template maven et cliquez sur "Apply".
![choisir le template maven](img/azure-devops-maven-template.png)

Votre pipeline aura alors un nouvel agent qui possède 3 étapes :
- Une étape maven qui est bien configurée par défaut.
- Une étape "Copy files to : $(build.artifactsstatgingdirectory)" dans laquelle nous devons marquer notre binaire.
Pour cela, configurez la variable "Content" à la valeur `target/azure-functions/**`
- Une étape "Publish Artifact : drop" dans laquelle vous pouvez modifier la valeur du champ "Artifact name".

![Configurer le chemin de l'Artefact maven](img/azure-devops-etapes-maven.png) 
 
### Ajouter l'étape de déploiement

Pour configurer le déploiement, éditez le pipeline et ajoutez une étape "Azure functions".

![Ajoutez l'étape Azure Functions](img/azure-devops-configure-function-deploy.png) 

Pour configurer l'étape :
- Choisissez votre souscription "Free Trial"
- Vous aurez peut-être à cliquer sur un bouton "Authorize" pour configurer une connexion de service azure
- Sélectionnez la valeur "App type" qui correspond à votre cas (linux ou windows)
- Sélectionnez la valeur "App name" que vous souhaitez mettre à jour. 
- Remplissez le champ "Package or folder" avec la valeur  `$(build.artifactstagingdirectory)/target/azure-functions/azure-function-tech-review`
- Cliquez maintenant sur "Save & queue" pour enregistrer la configuration. 

![Configurez l'étape Azure Functions](img/azure-devops-configure-step-deploy.png) 

Vous pouvez lancer un build pour vérifier que le pipeline fonctionne bien. 


