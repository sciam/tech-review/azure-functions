# Installer le JDK 11 Zulu for Azure

Depuis le site  https://www.azul.com/downloads/azure-only/zulu/?version=java-11-lts, télécharger et installer
la version Java 11 JDK compatible avec votre environnement de développement.  

# Install the Azure Functions Core Tools

A partir de repo https://github.com/Azure/azure-functions-core-tools/blob/master/README.md, Téléchargez et installez la
version de azure functions core tools compatible avec votre environnement. 

# Installer intellIJ

Vous pouvez télécharger et installer la dernière version d'intellIJ community edition gratuitement
depuis le site https://www.jetbrains.com/idea/download/.

Ouvrez IntellIJ et Installez le plugin azure toolkit for intellIJ. 

Cliquez sur "File" > puis "Settings..." > puis "Plugins" > chercher "Azure toolkit for intellIJ" > cliquez sur "Install".

![installez le plugin azure toolkit for intellIJ ](img/intelij-plugin-azure.png)

Vérifiez que la version de Maven est bien >= 3.0.5 

Cliquez sur "File" > puis "Settings..." > puis "Build, Execution, Deployment" > puis "Buidl Tools" > puis "Maven"

![Vérifier la version de Maven ](img/intelij-check-maven.png)

# Installer le "Azure storage Explorer" 

Vous pouvez télécharger l'installateur à partir de ce lien: https://azure.microsoft.com/en-us/features/storage-explorer/
