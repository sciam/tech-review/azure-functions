# Créer un compte Cloud Azure

Créer un compte gratuitement

Rendez-vous sur le portail du Cloud Azure pour créer un compte gratuitement : https://azure.microsoft.com/en-us/free/ 
Cliquez sur « Start free »

![Démarrez avec Azure](img/start-free.png) 

Créez votre compte. Remplissez le formulaire. Attention un numéro de carte est demandé cependant la carte ne sera pas débitée.

![Remplir le formulaire azure (1)](img/create-account.png)

![Remplir le formulaire azure (2)](img/sign-up.png)

Selectionnez votre fuseau horaire:
![Remplir le formulaire azure (3)](img/select-timezone.png)


# Votre compte est prêt!

![Remplir le formulaire azure (3)](img/account-ready.png)


